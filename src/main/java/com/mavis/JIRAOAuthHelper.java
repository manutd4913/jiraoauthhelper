package com.mavis;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @since v1.0
 * Follow the step to get your CONSUMER_KEY and CONSUMER_PRIVATE_KEY
 * https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-oauth-authentication
 */
public class JIRAOAuthHelper
{


    private static final String CALLBACK_URI = "http://consumer/callback";
    protected static final String CONSUMER_KEY = "Get you consumer key and paste here.";
    protected static final String CONSUMER_PRIVATE_KEY = "Get you private key and paste here.";
    protected static final String JIRA_BASEURL = "http://xxx.xxx.xxx.xxx:8080/rest/api/2/";

    public String runJIRAWithUserNameAuth(String route, String method, String data, String userName) throws Exception{
        AtlassianOAuthClient jiraoAuthClient = new AtlassianOAuthClient(CONSUMER_KEY, CONSUMER_PRIVATE_KEY, null, CALLBACK_URI);
        String url = route + method +"?xoauth_requestor_id="+ userName +"&";

        InputStream stream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
        String response = null;
        try {
            response = jiraoAuthClient.makeAuthenticatedRequest(url, "", stream);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return response;
    }

    public static void main(String[] args) throws Exception {

        JIRAOAuthHelper helper = new JIRAOAuthHelper();
        String taskId = "task_id";
        String data = "This is a comment.";
        String userName = "jira_accont";

        // This is a comment example
        helper.runJIRAWithUserNameAuth(JIRA_BASEURL, "issue/"+ taskId +"/comment", "{\"body\": \""+ data +"\"}", userName);

    }
}
